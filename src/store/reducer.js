import { createSlice } from '@reduxjs/toolkit'

const initialState = {

    isActive: false,
    isPaused: true,
    timer: 0,
    start:0,
    laps: []
}


const stopWatchSlice = createSlice({

    name: 'stopwatch',
    initialState: initialState,

    reducers:{
        startTimer(state,action){
           state.isActive = true;
           state.isPaused = false;
           state.timer += action.payload;

        },
        resetTimer(state){

           state.timer = 0;
           
        },
        lapCount(state){
            state.laps.push(state.timer)
        },

        StopTimer(state){
            state.isActive = false;
            state.isPaused = true;
        }
    }

})

export const stopWatchActions = stopWatchSlice.actions

export default stopWatchSlice.reducer