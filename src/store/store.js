import { configureStore } from '@reduxjs/toolkit'

import stopWatchReducer from './reducer'


const store = configureStore({
    reducer :{ reducer : stopWatchReducer}
})

export default store