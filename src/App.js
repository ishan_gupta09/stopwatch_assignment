import StopWatchTimer from './components/StopWatchTimer/StopWatchTimer';
import './App.css';
import StartButton from './components/StartButton/StartButton';
import LapButton from './components/LapButton/LapButton';
import ResetButton from './components/ResetButton/ResetButton';
import StopButton from './components/StopButton/StopButton';
import Laps from './components/Laps/Laps';

function App() {
  return (
    <div className="App">


    <h2>STOPWATCH</h2>
     
     <StopWatchTimer />

     <div className="buttons">

       <StartButton />
       <LapButton />
       <ResetButton />
       <StopButton />
     </div>

     <div>
       <Laps />
     </div>
    </div>
  );
}

export default App;
