import React from 'react'
import {useDispatch} from 'react-redux'
import{ stopWatchActions } from '../../store/reducer'
import '../UI/button.css'
const StartButton = () => {

    const dispatch = useDispatch();

 

    const startHandler = (event) => {

        event.preventDefault();

        dispatch(stopWatchActions.startTimer(0))

    }

    return (
        <button onClick={startHandler} className='btn'>
            Start
        </button>
    )
}

export default StartButton
