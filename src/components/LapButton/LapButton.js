import React from 'react'

import {useDispatch} from 'react-redux'

import{ stopWatchActions } from '../../store/reducer'

import '../UI/button.css'

const LapButton = () => {

    const dispatch = useDispatch() 

    const lapHandler = (event) => {

        event.preventDefault() ;

        dispatch(stopWatchActions.lapCount())
    }

    return (
      <button onClick={lapHandler} className='btn'>
        Lap
      </button>
    )
}

export default LapButton
