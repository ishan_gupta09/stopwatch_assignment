import React,{useEffect} from 'react'
import {useSelector,useDispatch} from 'react-redux'
import{ stopWatchActions } from '../../store/reducer'
import './StopWatchTimer.css'

const StopWatchTimer = () => {

    const dispatch = useDispatch()
    
   
    const {isActive, isPaused, timer } = useSelector((state) => state.reducer )

    useEffect(() => {
        let interval = null;
  
        if (isActive && isPaused === false) {
          interval = setInterval(() => {
        
          dispatch(stopWatchActions.startTimer(10))
          }, 10);
        } else {
          clearInterval(interval);
        }
        return () => {
          clearInterval(interval);
        };

    },[isActive,isPaused,dispatch])

    return (
        <div className="timer">
        <span className="digits">
          {("0" + Math.floor((timer / 1440000) % 24)).slice(-2)}  <span className="cols">:</span>
        </span>
        <span className="digits">
          {("0" + Math.floor((timer / 60000) % 60)).slice(-2)}   <span className="cols">:</span>
        </span>
        <span className="digits">
          {("0" + Math.floor((timer / 1000) % 60)).slice(-2)}  <span className="cols">:</span>
        </span>
        <span className="digits mili-sec">
          {("0" + ((timer / 10) % 100)).slice(-2)}
        </span>
      </div>
    )
}

export default StopWatchTimer
