import React from 'react'

import {useDispatch} from 'react-redux'

import{ stopWatchActions } from '../../store/reducer'

import '../UI/button.css'

const StopButton = () => { 

    const dispatch = useDispatch()

    const stopHandler = (event) =>{

        event.preventDefault();

        dispatch(stopWatchActions.StopTimer())
    }
    return (
        <button onClick={stopHandler} className='btn'>
            Stop
        </button>
    )
}

export default StopButton
