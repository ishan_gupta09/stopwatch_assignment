import React from 'react'

import {useDispatch, useSelector} from 'react-redux'

import{ stopWatchActions } from '../../store/reducer'

import '../UI/button.css'
const ResetButton = () => {

    const dispatch = useDispatch();

    const {isActive} = useSelector((state) => state.reducer)

    const resetHandler = (event) =>{
        
        event.preventDefault();
        dispatch(stopWatchActions.resetTimer())
    }

    return (
        <button onClick={resetHandler} disabled={isActive} className='btn'>
            Reset 
        </button>
    )
}

export default ResetButton
