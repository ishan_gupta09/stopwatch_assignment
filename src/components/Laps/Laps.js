import React from 'react'

import {useDispatch , useSelector} from 'react-redux'

import {stopWatchActions} from '../../store/reducer'

import './Laps.css'

const Laps = () => {

    const dispatch = useDispatch()

    const {laps} = useSelector((state) => state.reducer)

    console.log(laps)

    return (
        <div className="laps__container">
            {laps.map((time) => (
                <div style={{overflow: 'hidden'}} >
                <span className="digits">
                {("0" + Math.floor((time / 60000) % 60)).slice(-2)} :
                </span>
                <span className="digits">
                {("0" + Math.floor((time / 1000) % 60)).slice(-2)} :
                </span>
                <span className="digits ">
                {("0" + ((time / 10) % 100)).slice(-2)}
                </span>
            </div>

            ) )}
        </div>
    )
}

export default Laps
